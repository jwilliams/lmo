package lmo.genotype;

import java.util.ArrayList;
import java.util.List;

import lmo.search.SearchContext;
import lmo.search.exception.InvalidOperationTypeException;

/**
 * Extension of the FiniteGeneList to include variable length individuals. The only methods 
 * which have been affected are initialise() and reproduce(). To accommodate variable length
 * two new properties have been introduced:
 * 	- lmo.population.initialisation.minListSize
 *  - lmo.population.initialisation.maxListSize
 * The initialise method will select a length randomly between these ranges.
 * 
 * See also lmo.test.VariableLengthGenotypeTest
 * 
 * @author James R. Williams
 *
 */
public class VariableLengthFiniteGeneList extends FiniteGeneList {

	public VariableLengthFiniteGeneList() {
		super();
	}

	public VariableLengthFiniteGeneList(FiniteGene[] genes) {
		super(genes);
	}
	
	public void initialise(String operation, SearchContext searchContext) throws InvalidOperationTypeException {

		String initialisationType = searchContext.getProperty(operation+".type");
		
		if (initialisationType.compareToIgnoreCase("random")==0) {
			
			// We're going to assume here, that if this Class has been chosen, then the
			// variable length properties have been set.
			int minListSize = searchContext.getPropertyInt(operation + ".minListSize");
			int maxListSize = searchContext.getPropertyInt(operation + ".maxListSize");
			
			// Need to add 1 here in case max and min are the same size
			int listSize = searchContext.getRandom().nextInt(maxListSize - minListSize + 1) + minListSize;
			int maxAllele = searchContext.getPropertyInt(operation+".maxAllele");

			genes = new FiniteGene[listSize];
			
			for (int a = 0; a < genes.length; a++) {
				genes[a] = new FiniteGene(searchContext.getRandom().nextInt(maxAllele+1),maxAllele);
			}
		}
		else {
			throw new InvalidOperationTypeException(operation, initialisationType);
		}
	}
	
	public List<Genotype> reproduce(String operation, Genotype partner, SearchContext searchContext) throws InvalidOperationTypeException {
		// NOTE: assumes that genotypes have been cloned from parents, i.e. we can change the genes directly

		String reproductionType = searchContext.getProperty(operation+".type");

		List<Genotype> children = new ArrayList<Genotype>();

		if (reproductionType.compareToIgnoreCase("singlePointCrossover")==0) {

			FiniteGeneList offspring1 = (FiniteGeneList)partner;
			FiniteGeneList offspring2 = (FiniteGeneList)this;
	
			// Select the crossover point to be within BOTH genomes.
			int maxCrossoverPoint;
			if (genes.length < ((VariableLengthFiniteGeneList)partner).genes.length) {
				maxCrossoverPoint = genes.length;
			} else{
				maxCrossoverPoint = ((VariableLengthFiniteGeneList)partner).genes.length;
			}
			
			int crossoverPoint = searchContext.getRandom().nextInt(maxCrossoverPoint + 1);
//			System.out.println("Crossover point: " + crossoverPoint);
			for (int a = 0; a < crossoverPoint; a++) {
				FiniteGene temp = offspring1.genes[a];
				offspring1.genes[a] = offspring2.genes[a];
				offspring2.genes[a] = temp;
			}
			
			children.add(offspring1);
			children.add(offspring2);
		}
		else {
			throw new InvalidOperationTypeException(operation, reproductionType);
		}

		return children;

	}
	
	public Object clone() {
		return new VariableLengthFiniteGeneList(genes);
	}

}
