package lmo.genotype;

import java.util.ArrayList;
import java.util.List;

import lmo.search.SearchContext;
import lmo.search.exception.InvalidOperationTypeException;

/**
 * Extension to the VariableLengthFiniteGeneList that is tailored towards evolution strategies. 
 * 
 * The main change is that the reproduce() method simply duplicates the parent Y times, the mutation
 * method can then do the evolutionary work.
 * 
 * TODO: This is definitely not a good implementation and is subject to outrageous misuse if certain
 * properties are set (or not set).
 * 
 * @author James R. Williams
 *
 */
public class VariableLengthNoCrossOverFiniteGeneList extends VariableLengthFiniteGeneList {

	public VariableLengthNoCrossOverFiniteGeneList() {
		super();
	}

	public VariableLengthNoCrossOverFiniteGeneList(FiniteGene[] genes) {
		super(genes);
	}
	
	public List<Genotype> reproduce(String operation, Genotype partner, SearchContext searchContext) throws InvalidOperationTypeException {
		// NOTE: assumes that there is a single parent, which is duplicated.
		List<Genotype> children = new ArrayList<Genotype>();
		
		int childPopulationSize = searchContext.getPropertyInt("lmo.childPopulation.size");
		
		for (int i = 0; i < childPopulationSize; i++) {
			children.add((Genotype)partner.clone());
		}
		
		return children;
	}
	
	public Object clone() {
		return new VariableLengthNoCrossOverFiniteGeneList(genes);
	}
}
