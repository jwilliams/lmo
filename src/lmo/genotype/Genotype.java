package lmo.genotype;

import java.util.List;
import lmo.search.SearchContext;
import lmo.search.exception.InvalidOperationTypeException;

public interface Genotype extends Cloneable {
	public void initialise(String operation, SearchContext searchContext) throws InvalidOperationTypeException;
	public void mutate(String operation, SearchContext searchContext) throws InvalidOperationTypeException;
	public List<Genotype> reproduce(String operation, Genotype partner, SearchContext searchContext) throws InvalidOperationTypeException;
	public Object clone();
	public String toString();
}
