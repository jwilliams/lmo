package lmo.genotype;

import lmo.search.SearchContext;
import lmo.search.exception.InvalidOperationTypeException;

import java.util.ArrayList;
import java.util.List;

public class FiniteGeneList implements Genotype {

	protected FiniteGene[] genes;

	public FiniteGeneList() {
		this.genes = new FiniteGene[0];
	}

	public FiniteGeneList(FiniteGene[] genes) {
		this.genes = new FiniteGene[genes.length];
		// need to explicitly do a deep copy
		for (int a = 0; a < genes.length; a++) {
			this.genes[a] = (FiniteGene)genes[a].clone();
		}
	}
	
	public FiniteGene[] getGenes() {
		return genes;
	}
	public void setGenes(FiniteGene[] genes) {
		this.genes = genes;
	}
	
	public void initialise(String operation, SearchContext searchContext) throws InvalidOperationTypeException {

		String initialisationType = searchContext.getProperty(operation+".type");
		
		if (initialisationType.compareToIgnoreCase("random")==0) {
	
			int listSize = searchContext.getPropertyInt(operation+".listSize");
			int maxAllele = searchContext.getPropertyInt(operation+".maxAllele");
			
			genes = new FiniteGene[listSize];
			
			for (int a = 0; a < genes.length; a++) {
				genes[a] = new FiniteGene(searchContext.getRandom().nextInt(maxAllele+1),maxAllele);
			}
		}
		else {
			throw new InvalidOperationTypeException(operation, initialisationType);
		}
		
	}

	public List<Genotype> reproduce(String operation, Genotype partner, SearchContext searchContext) throws InvalidOperationTypeException {
		// NOTE: assumes that genotypes have been cloned from parents, i.e. we can change the genes directly

		String reproductionType = searchContext.getProperty(operation+".type");

		List<Genotype> children = new ArrayList<Genotype>();

		if (reproductionType.compareToIgnoreCase("singlePointCrossover")==0) {

			FiniteGeneList offspring1 = (FiniteGeneList)partner;
			FiniteGeneList offspring2 = (FiniteGeneList)this;
	
			int crossoverPoint = searchContext.getRandom().nextInt(genes.length+1);
			for (int a = 0; a < crossoverPoint; a++) {
				FiniteGene temp = offspring1.genes[a];
				offspring1.genes[a] = offspring2.genes[a];
				offspring2.genes[a] = temp;
			}
			
			children.add(offspring1);
			children.add(offspring2);
		}
		else {
			throw new InvalidOperationTypeException(operation, reproductionType);
		}

		return children;

	}
	
	public void mutate(String operation, SearchContext searchContext) throws InvalidOperationTypeException {
		// NOTE: assumes that genotype has been cloned from parent, i.e. we can change the genes directly
		
		String mutationType = searchContext.getProperty(operation+".type");
		double probability = searchContext.getPropertyDouble(operation+ ".probability");

		if (mutationType.compareToIgnoreCase("random")==0) {

			for (int a = 0; a < genes.length; a++) {
				if (searchContext.getRandom().nextDouble() < probability) {
					genes[a].setAllele(searchContext.getRandom().nextInt(genes[a].getMaxAllele()+1));
				}
			}
		}
		else if (mutationType.compareToIgnoreCase("incremental")==0) {
			
			for (int a = 0; a < genes.length; a++) {
				int delta = 0;
				if (searchContext.getRandom().nextDouble() < probability) {
					if (genes[a].getAllele() == 0) {
						delta = 1;
					}
					else if (genes[a].getAllele() == genes[a].getMaxAllele()) {
						delta = -1;
					}
					else if (searchContext.getRandom().nextBoolean()) {
						delta = 1;
					}
					else {
						delta = -1;
					}
				}
				genes[a].setAllele(genes[a].getAllele() + delta);
			}

		}
		else {
			throw new InvalidOperationTypeException(operation, mutationType);
		}

	}
	
	public String toString() {
		String string = "[ ";
		for (FiniteGene gene : genes) {
			string += gene.toString() + " ";
		}
		string += "]";
		return string;
	}
	
	public Object clone() {
		return new FiniteGeneList(genes);
	}

	
}
