package lmo.genotype;

public class FiniteGene implements Cloneable {

	private int allele;
	private int maxAllele;
	
	public FiniteGene(int allele, int maxAllele) {
		this.allele = allele;
		this.maxAllele = maxAllele;
	}
	
	public int getAllele() {
		return allele;
	}
	
	public void setAllele(int allele) {
		this.allele = allele;
	}
	
	public int getMaxAllele() {
		return maxAllele;
	}

	public void setMaxAllele(int maxAllele) {
		this.maxAllele = maxAllele;
	}
	
	public String toString() {
		return String.valueOf(allele);
	}

	public Object clone() {
		return new FiniteGene(allele, maxAllele);
	}

	
}
