package lmo.test;

import lmo.fitness.QuantitativeFitnessFunction;
import lmo.genotype.FiniteGene;
import lmo.genotype.FiniteGeneList;
import lmo.genotype.Genotype;
import java.util.Properties;

public class OneMaxFitnessFunction extends QuantitativeFitnessFunction {

	public double quantifyGenotype(Genotype genotype, Properties fitnessFunctionProps) {
		
		FiniteGene[] genes = ((FiniteGeneList)genotype).getGenes();
		double fitness = 0;
		for (FiniteGene gene : genes) {
			if (gene.getAllele() !=1 )
				fitness += 1;
		}
		return fitness;
	}

}
