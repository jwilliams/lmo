package lmo.test;

import java.util.List;
import java.util.Properties;

import lmo.genotype.FiniteGene;
import lmo.genotype.Genotype;
import lmo.genotype.VariableLengthFiniteGeneList;
import lmo.search.SearchContext;
import lmo.search.exception.InvalidOperationTypeException;

import org.junit.Before;
import org.junit.Test;


public class VariableLengthGenotypeTest {

	VariableLengthFiniteGeneList parent1, parent2, parent3;
	SearchContext searchContext;
	
	@Before
	public void setup(){
		FiniteGene[] genes1 = new FiniteGene[]{	// All 1s
				new FiniteGene(1,1),new FiniteGene(1,1),new FiniteGene(1,1),new FiniteGene(1,1),};
		FiniteGene[] genes2 = new FiniteGene[]{ // All 0s
				new FiniteGene(0,1),new FiniteGene(0,1),new FiniteGene(0,1),new FiniteGene(0,1),};
		FiniteGene[] genes3 = new FiniteGene[]{ // All 0s
				new FiniteGene(0,1),new FiniteGene(0,1),new FiniteGene(0,1),new FiniteGene(0,1),new FiniteGene(0,1),new FiniteGene(0,1),};
		
		parent1 = new VariableLengthFiniteGeneList(genes1);
		parent2 = new VariableLengthFiniteGeneList(genes2);
		parent3 = new VariableLengthFiniteGeneList(genes3);
		
		Properties props = new Properties();
		props.put("lmo.parentPopulation.reproduction.type", "singlePointCrossover");
		props.put("lmo.searchContext.usePrngSeed","false");
		searchContext = new SearchContext(props);
	}
	
	@Test
	public void testSameLengthCrossover() throws InvalidOperationTypeException {
		System.out.println("Same length parents:");
		
		System.out.println("Parent 1: " + parent1.toString());
		System.out.println("Parent 2: " + parent2.toString());
		
		List<Genotype> children = parent1.reproduce("lmo.parentPopulation.reproduction", parent2, searchContext);
		
		System.out.println("Child 1: " + children.get(0).toString());
		System.out.println("Child 2: " + children.get(1).toString());
	}
	
	@Test
	public void testDifferentLengthCrossover() throws InvalidOperationTypeException {
		System.out.println("Different length parents:");
		
		System.out.println("Parent 1: " + parent1.toString());
		System.out.println("Parent 3: " + parent3.toString());
		
		List<Genotype> children = parent1.reproduce("lmo.parentPopulation.reproduction", parent3, searchContext);
		
		System.out.println("Child 1: " + children.get(0).toString());
		System.out.println("Child 2: " + children.get(1).toString());
		
		int shortestLengthParent = parent1.getGenes().length;
		
		// Because we are crossing over zeros and ones, we should always sum to 1. The remainder will be zero.
		for (int i = 0; i < shortestLengthParent; i++) {
			int allele1 = ((VariableLengthFiniteGeneList)children.get(0)).getGenes()[i].getAllele();
			int allele2 = ((VariableLengthFiniteGeneList)children.get(1)).getGenes()[i].getAllele();
			assert(allele1 + allele2 == 1);
		}
	}
}
