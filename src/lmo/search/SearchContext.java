package lmo.search;

import java.util.Random;
import java.util.Properties;

import lmo.search.exception.InvalidOperationTypeException;

public class SearchContext {
	
	private Properties searchProps;
	private Random random;
	private int iterations;
	private int quantifications;
	private int comparisons;
	
	public SearchContext(Properties searchProps) {
	
		this.searchProps = searchProps;
		
		random = new Random();
		if (getPropertyBoolean("lmo.searchContext.usePrngSeed"))
		{
			random.setSeed(getPropertyLong("lmo.searchContext.prngSeed"));
		}
		
		iterations = 0;
		quantifications = 0;
		comparisons = 0;
		
	}
	
	public Properties getSearchProperties() {
		return searchProps;
	}

	public String getProperty(String name) {
		return searchProps.getProperty(name);
	}

	public int getPropertyInt(String name) {
		return Integer.valueOf(getProperty(name));
	}

	public long getPropertyLong(String name) {
		return Long.valueOf(getProperty(name));
	}

	public double getPropertyDouble(String name) {
		return Double.valueOf(getProperty(name));
	}

	public boolean getPropertyBoolean(String name) {
		return getProperty(name).compareToIgnoreCase("true")==0;
	}

	public Random getRandom() {
		return random;
	}
	
	public void incrementIterations() {
		iterations++;
	}
	
	public int getIterations() {
		return iterations;
	}

	public void incrementQuantifications() {
		quantifications++;
	}

	public void incrementComparisons() {
		comparisons++;
	}

	public boolean terminate(String operation) throws InvalidOperationTypeException {

		boolean terminate = false;
		
		String terminationType = getProperty(operation+".type");
		
		if (terminationType.compareToIgnoreCase("iterations")==0) {
			
			int iterationsThreshold = getPropertyInt(operation+".iterationsThreshold");

			terminate = (iterations >= iterationsThreshold);
			
			if (terminate && getPropertyBoolean(operation+".log")) {
				System.out.println(operation + ": iterations of " + iterations + " is at or over threshold\n");
			}

			
		}
		else if (terminationType.compareToIgnoreCase("quantifications")==0) {
			
			int quantificationsThreshold = getPropertyInt(operation+".quantificationsThreshold");

			terminate = (quantifications >= quantificationsThreshold);
			
			if (terminate && getPropertyBoolean(operation+".log")) {
				System.out.println(operation + ": quantifications of " + quantifications + " is at or over threshold\n");
			}

			
		}
		else {
			throw new InvalidOperationTypeException(operation, terminationType);
		}

		return terminate;
		
	}
	
	public String toString()
	{
		String string = "";
		string += String.format("Iterations %d\n", iterations);
		string += String.format("Quantifications %d\n", quantifications);
		string += String.format("Comparisons %d\n", comparisons);
		return string;
	}
	
	public void log(String operation) {
		
		if (getPropertyBoolean(operation + ".log"))
			System.out.println(operation + ":\n" + this);

	}

}
