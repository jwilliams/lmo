package lmo.search;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;
import java.util.Comparator;

import lmo.fitness.ComparativeFitnessFunction;
import lmo.fitness.QuantitativeFitnessFunction;
import lmo.genotype.Genotype;
import lmo.search.exception.DirectGenotypeComparisonNotSupportedException;
import lmo.search.exception.ExpiredFitnessException;
import lmo.search.exception.InvalidFitnessException;
import lmo.search.exception.InvalidOperationTypeException;

public class Population {

	private List<Individual> individualList;
	
	Population() {
		individualList = new ArrayList<Individual>();
	}

	public List<Individual> getIndividualList() {
		return individualList;
	}

	public void setIndividualList(List<Individual> population) {
		this.individualList = population;
	}

	public boolean add(Individual individual) {
		return individualList.add(individual);
	}

	public boolean add(Population population) {
		return this.individualList.addAll(population.getIndividualList());
	}

	public int size() {
		return individualList.size();
	}

	public Individual get(int index) {
		return individualList.get(index);
	}

	public Genotype getGenotypeClone(int index) {
		return get(index).getGenotypeClone();
	}

	public Individual pick(Random random) {
		return individualList.get(random.nextInt(individualList.size()));
	}

	public Genotype pickGenotypeClone(Random random)  {
		return pick(random).getGenotypeClone();
	}
	
	public String toString()
	{
		String string = "";
		for(Individual individual : individualList) {
			string += individual.toString() + "\n";
		}
		return string;
	}
	
	public Population select(String operation, int selectNumber, SearchContext searchContext) throws InvalidOperationTypeException, InvalidFitnessException, DirectGenotypeComparisonNotSupportedException {
		
		String selectionType = searchContext.getProperty(operation+".type");
		
		Population selectedPopulation = new Population();

		if (selectionType.compareToIgnoreCase("copy")==0) {
			
			int multiples = selectNumber / individualList.size();
			
			for (int m = 0; m < multiples; m++) {
				for (int i = 0; i < individualList.size(); i++) {
					selectedPopulation.add((Individual)individualList.get(i).clone());
				}
			}
			
		}
		else if (selectionType.compareToIgnoreCase("random")==0) {
			
			for (int i = 0; i < selectNumber; i++) {
				selectedPopulation.add((Individual)individualList.get(searchContext.getRandom().nextInt(individualList.size())).clone());
			}
			
		}
		else if (selectionType.compareToIgnoreCase("tournament")==0) {
			
			ComparativeFitnessFunction comparator = null;
			try {
				comparator = (ComparativeFitnessFunction)Class.forName(searchContext.getProperty("lmo.fitnessFunction.class")).newInstance();
			} catch (Exception e) {
				e.printStackTrace();		
			}
			
			int tournamentSize = searchContext.getPropertyInt(operation+".tournamentSize");
			
			for (int i = 0; i < selectNumber; i++) {
				List<Integer> contestants = new ArrayList<Integer>();
				
				for (int t = 0; t < tournamentSize; t++) {
					contestants.add(new Integer(searchContext.getRandom().nextInt(individualList.size())));
				}
				
				while (contestants.size() > 1) {
					
					for (int c = 0; c < (contestants.size()-1); c++) {
						
						int comparison = comparator.compareIndividuals(
								get(contestants.get(c)),
								get(contestants.get(c+1)),
								searchContext);
						
						if (comparison==0) {
							if (searchContext.getRandom().nextBoolean()) {
								comparison = 1;
							}
						}
						if (comparison < 0) {
							contestants.remove(c+1);
						}
						else {
							contestants.remove(c);
						}
					}
					
				}
				
				selectedPopulation.add((Individual)individualList.get(contestants.get(0)).clone());

			}

		}
		else if (selectionType.compareToIgnoreCase("fitnessProportional")==0) {
			
			QuantitativeFitnessFunction quantifier = null;
			try {
				quantifier = (QuantitativeFitnessFunction)Class.forName(searchContext.getProperty("lmo.fitnessFunction.class")).newInstance();
			} catch (Exception e) {
				e.printStackTrace();		
			}

			double[] fitnesses = new double[size()];
			// normalised fitness is between 0 and 1, with 1 being the best
			double maxFitness = 0.0;
			boolean first = true;
			for (int j = 0; j < size(); j++) {
				double fitness = quantifier.quantifyIndividual(get(j), searchContext);
				if (fitness < 0.0)
					throw new InvalidFitnessException();
				if (first || (fitness > maxFitness)) {
					maxFitness = fitness;
					first = false;
				}
				fitnesses[j] = fitness;
			}
			
			double[] cumulativeNormalisedFitnesses = new double[size()];
			double cumulativeNormalisedFitness = 0.0;
			for (int j = 0; j < size(); j++) {
				double normalisedFitness = 1.0;
				if (maxFitness > 0.0) {
					normalisedFitness = 1.0 - (fitnesses[j] / maxFitness);
				}
				cumulativeNormalisedFitness += normalisedFitness;
				cumulativeNormalisedFitnesses[j] = cumulativeNormalisedFitness;
			}
				
			for (int i = 0; i < selectNumber; i++) {
				
				double target = searchContext.getRandom().nextFloat() * cumulativeNormalisedFitness;

				// defensive in case of rounding errors
				if (!(target <= cumulativeNormalisedFitness)) {
					target = cumulativeNormalisedFitness;
				}
				
				for (int j = 0; j < size(); j++) {
					if (target <= cumulativeNormalisedFitnesses[j]) {
						selectedPopulation.add((Individual)individualList.get(j).clone());
						break;
					}
				}
			}

		}
		else if (selectionType.compareToIgnoreCase("fittest")==0) {
						
			QuantitativeFitnessFunction quantifier = null;
			try {
				quantifier = (QuantitativeFitnessFunction)Class.forName(searchContext.getProperty("lmo.fitnessFunction.class")).newInstance();
			} catch (Exception e) {
				e.printStackTrace();		
			}

			class IndividualFitness {
				int idx;
				double fitness;
				double rnd;
			}

			List<IndividualFitness> individualFitnesses = new ArrayList<IndividualFitness>(); 
			for (int j = 0; j < size(); j++) {
				IndividualFitness individualFitness = new IndividualFitness();
				individualFitness.idx = j;
				individualFitness.fitness = quantifier.quantifyIndividual(get(j), searchContext);
				if (individualFitness.fitness < 0.0)
					throw new InvalidFitnessException();
				individualFitness.rnd = searchContext.getRandom().nextFloat();
				individualFitnesses.add(individualFitness);
				// rnd is used to randomly sort individuals with the same fitness
			}

			Collections.sort(individualFitnesses, 
				new Comparator<IndividualFitness>() {
		          public int compare(IndividualFitness if1, IndividualFitness if2) {
		        	  double diff = if1.fitness - if2.fitness;
		        	  if (diff == 0)
		        		  diff = if1.rnd - if2.rnd;
		        	  return (int)Math.signum(diff);
		          } });

			for (int i = 0; i < selectNumber; i++) {
				selectedPopulation.add((Individual)get(individualFitnesses.get(i).idx).clone());
			}
			
		}
		else {
			throw new InvalidOperationTypeException(operation, selectionType);
		}
					
		return selectedPopulation;
		
	}

	public boolean terminate(String operation, SearchContext searchContext) throws ExpiredFitnessException, InvalidFitnessException, InvalidOperationTypeException {
		
		boolean terminate = false;
		
		String terminationType = searchContext.getProperty(operation+".type");
		
		if (terminationType.compareToIgnoreCase("bestFitness")==0) {
			
			QuantitativeFitnessFunction quantifier = null;
			try {
				quantifier = (QuantitativeFitnessFunction)Class.forName(searchContext.getProperty("lmo.fitnessFunction.class")).newInstance();
			} catch (Exception e) {
				e.printStackTrace();		
			}
			
			double fitnessThreshold = searchContext.getPropertyDouble(operation+".fitnessThreshold");

			boolean first = true;
			double bestFitness = 0.0;
			for (int j = 0; j < size(); j++) {
				double fitness = quantifier.quantifyIndividual(get(j), searchContext);
				if (first || (fitness < bestFitness)) {
					bestFitness = fitness;
					first = false;
				}
			}
			
			terminate = (bestFitness <= fitnessThreshold);
			
			if (terminate && searchContext.getPropertyBoolean(operation+".log")) {
				System.out.println(operation + ": best fitness of " + bestFitness + " is at or under threshold\n");
			}

		}
		else if (terminationType.compareToIgnoreCase("iqrFitness")==0) {
			
			QuantitativeFitnessFunction quantifier = null;
			try {
				quantifier = (QuantitativeFitnessFunction)Class.forName(searchContext.getProperty("lmo.fitnessFunction.class")).newInstance();
			} catch (Exception e) {
				e.printStackTrace();		
			}
			
			double iqrThreshold = searchContext.getPropertyDouble(operation+".iqrThreshold");
			
			double[] fitnesses = new double[size()];
			for (int j = 0; j < size(); j++) {
				fitnesses[j] = quantifier.quantifyIndividual(get(j), searchContext);
			}
			
			Arrays.sort(fitnesses);
			
			int quartileIdx = size() / 4;
			double iqr = fitnesses[size()-1-quartileIdx] - fitnesses[quartileIdx-1];
			
			terminate = (iqr <= iqrThreshold);

			if (terminate && searchContext.getPropertyBoolean(operation+".log")) {
				System.out.println(operation + ": iqr of " + iqr + " is at or under threshold\n");
			}
			
		}
		else if (terminationType.compareToIgnoreCase("worstFitness")==0) {

			QuantitativeFitnessFunction quantifier = null;
			try {
				quantifier = (QuantitativeFitnessFunction)Class.forName(searchContext.getProperty("lmo.fitnessFunction.class")).newInstance();
			} catch (Exception e) {
				e.printStackTrace();		
			}
			
			double fitnessThreshold = searchContext.getPropertyDouble(operation+".fitnessThreshold");

			boolean first = true;
			double worstFitness = 0.0;
			for (int j = 0; j < size(); j++) {
				double fitness = quantifier.quantifyIndividual(get(j), searchContext);
				if (first || (fitness > worstFitness)) {
					worstFitness = fitness;
					first = false;
				}
			}
			
			terminate = (worstFitness <= fitnessThreshold);
			
			if (terminate && searchContext.getPropertyBoolean(operation+".log")) {
				System.out.println(operation + ": worst fitness of " + worstFitness + " is at or under threshold\n");
			}


		}
		else {
			throw new InvalidOperationTypeException(operation, terminationType);
		}

		return terminate;
	}
	
	public void log(String operation, SearchContext searchContext) {
	
		if (searchContext.getPropertyBoolean(operation + ".log"))
			System.out.println(operation + ":\n" + this);
	}
	
}
