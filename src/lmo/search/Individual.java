package lmo.search;

import lmo.genotype.Genotype;
import lmo.search.exception.ExpiredFitnessException;
import lmo.search.exception.InvalidOperationTypeException;

public class Individual implements Cloneable {
	
	private Genotype genotype;
	private double fitness = 0;
	private boolean expiredFitness = true;
	
	Individual(Genotype genotype) {
		this.genotype = genotype;
		this.fitness = 0;
		this.expiredFitness = true;
		// Note: no cloning here
		// Cloning logic is that the Individual looks after its own: in other words, whenever the genotype is accessed (e.g. 
		// via get method or Individual constructed from another individual, the genotype is cloned.
		// In other cases (e.g. set method, or individual constructed from genotype), a copy is performed and we assume
		// that calling method handles any necessary cloning beforehand.
	}

	Individual(Individual individual) {
		this.genotype = (Genotype)individual.genotype.clone();
		this.fitness = individual.fitness;
		this.expiredFitness = individual.expiredFitness;
		// Note: cloning here (also required for cloning of individual)
	}

	public Genotype getGenotypeClone() {
		return (Genotype)genotype.clone();
	}

	public void initialise(String operation, SearchContext searchContext) throws InvalidOperationTypeException {
		genotype.initialise(operation, searchContext);
		expireFitness();
	}

	public void mutate(String operation, SearchContext searchContext) throws InvalidOperationTypeException {
		genotype.mutate(operation, searchContext);
		expireFitness();
	}

	public void setGenotype(Genotype genotype) {
		this.genotype = genotype;
		expireFitness();
	}

	public double getFitness() throws ExpiredFitnessException {
		if (expiredFitness)
			throw new ExpiredFitnessException();
		return fitness;
	}

	public void setFitness(double fitness) {
		expiredFitness = false;
		this.fitness = fitness;
	}

	public boolean isFitnessExpired() {
		return expiredFitness;
	}

	public void expireFitness() {
		fitness = 0.0; // not necessary, but used defensively
		expiredFitness = true;
	}

	public String toString() {
		String str = genotype.toString();
		if (!expiredFitness)
			str += " (Fitness: " + fitness + ")";
		return str;
	}
	
	public Object clone() {
		return new Individual(this);
	}
	
	public void log(String operation, SearchContext searchContext) {
		
		if (searchContext.getPropertyBoolean(operation + ".log"))
			System.out.println(operation + ":\n" + this);
	}
}
