package lmo.search.exception;

@SuppressWarnings("serial")
public class InvalidOperationTypeException extends SearchException {

	private String operation;
	private String type;
	
	public InvalidOperationTypeException(String operation, String type) {
		this.operation = operation;
		this.type = type;
	}

	@Override
	public String getMessage() {
		return String.format("For operation %s the type %s is invalid", operation, type);
	}
	
}