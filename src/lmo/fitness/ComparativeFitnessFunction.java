package lmo.fitness;

import java.util.Properties;
import lmo.genotype.Genotype;
import lmo.search.Individual;
import lmo.search.SearchContext;
import lmo.search.exception.DirectGenotypeComparisonNotSupportedException;

public abstract class ComparativeFitnessFunction {
	
	public int compareIndividuals(Individual individual1, Individual individual2, SearchContext searchContext) throws DirectGenotypeComparisonNotSupportedException {
		searchContext.incrementComparisons();
		return compareGenotypes(individual1.getGenotypeClone(),individual2.getGenotypeClone(), searchContext.getSearchProperties());
	}
	
	public abstract int compareGenotypes(Genotype genotype1, Genotype genotype2, Properties fitnessFunctionProps) throws DirectGenotypeComparisonNotSupportedException;
}
