package lmo.fitness;

import java.util.Properties;
import lmo.genotype.Genotype;
import lmo.search.Individual;
import lmo.search.SearchContext;
import lmo.search.exception.ExpiredFitnessException;
import lmo.search.exception.DirectGenotypeComparisonNotSupportedException;


public abstract class QuantitativeFitnessFunction extends ComparativeFitnessFunction {

	public double quantifyIndividual(Individual individual, SearchContext searchContext) {
		if (individual.isFitnessExpired()) {
			individual.setFitness(quantifyGenotype(individual.getGenotypeClone(), searchContext.getSearchProperties()));
			searchContext.incrementQuantifications();
		}
		double fitness = 0;
		try {
			fitness = individual.getFitness();
		}
		catch (ExpiredFitnessException e) {
			e.printStackTrace();		
		}
		return fitness;
	}
	
	public int compareIndividuals(Individual individual1, Individual individual2, SearchContext searchContext) {
		
		if (individual1.isFitnessExpired()) {
			individual1.setFitness(quantifyGenotype(individual1.getGenotypeClone(), searchContext.getSearchProperties()));
			searchContext.incrementQuantifications();
		}
		if (individual2.isFitnessExpired()) {
			individual2.setFitness(quantifyGenotype(individual2.getGenotypeClone(), searchContext.getSearchProperties()));
			searchContext.incrementQuantifications();
		}
		int ret = 0;
		try {
			ret = (int)Math.signum(individual1.getFitness() - individual2.getFitness());
			searchContext.incrementComparisons();
		}
		catch (ExpiredFitnessException e) {
			e.printStackTrace();		
		}
		return ret;
	}
	
	public int compareGenotypes(Genotype genotype1, Genotype genotype2, Properties fitnessFunctionProps) throws DirectGenotypeComparisonNotSupportedException {
		throw new DirectGenotypeComparisonNotSupportedException();
	}

	public abstract double quantifyGenotype(Genotype genotype, Properties searchProperties);
	
	
}
